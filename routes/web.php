<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

$this->group([
              'middleware'  => ['auth'],
              'namespace'   =>  'Admin',
              'prefix'      =>  'admin' 
             ],function() { 


Route::resource('home', 'HomeController');
Route::resource('setores', 'SetorController');
Route::resource('unidades', 'UnidadeController');
Route::resource('horarios', 'HorarioController');


});