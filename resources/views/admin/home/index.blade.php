@extends('admin.layouts.app')


@section('content')


    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div>

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
   
    <section class="content">


      <div class="container-fluid">       
        <div class="row">

            <div class="col-lg-3 col-6">            
              <div class="small-box bg-info-gradient">
                <div class="inner">
                  <h3>150</h3>
                  <p>New Orders</p>
                </div>
               <div class="icon">
                 <i class="ion ion-cash"></i>
               </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>          

           <div class="col-lg-3 col-6">            
              <div class="small-box bg-info-gradient">
                <div class="inner">
                  <h3>150</h3>
                  <p>New Orders</p>
                </div>
               <div class="icon">
                 <i class="ion ion-cash"></i>
               </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div> 
      
        </div>  
      </div>

    <!-- ANIVERSARIANTES --> 
      <div class="row">
             <div class="col-md-12">               
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Aniversariantes do Mês</h3>

                    <div class="card-tools">
                      <span class="badge badge-danger">12 Aniversariantes</span>                     
                      </button>                   
                    </div>
                  </div>
                  
                  <div class="card-body p-0">
                    <ul class="users-list clearfix">
                      <li>
                        <img src="../../dist/img/user1-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Alexander Pierce</a>
                        <span class="users-list-date">Today</span>
                      </li>
                      <li>
                        <img src="../../dist/img/user8-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Norman</a>
                        <span class="users-list-date">Yesterday</span>
                      </li>
                      <li>
                        <img src="../../dist/img/user7-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Jane</a>
                        <span class="users-list-date">12 Jan</span>
                      </li>
                      <li>
                        <img src="../../dist/img/user6-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">John</a>
                        <span class="users-list-date">12 Jan</span>
                      </li>
                      <li>
                        <img src="../../dist/img/user2-160x160.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Alexander</a>
                        <span class="users-list-date">13 Jan</span>
                      </li>
                      <li>
                        <img src="../../dist/img/user5-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Sarah</a>
                        <span class="users-list-date">14 Jan</span>
                      </li>
                      <li>
                        <img src="../../dist/img/user4-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Nora</a>
                        <span class="users-list-date">15 Jan</span>
                      </li>
                      <li>
                        <img src="../../dist/img/user3-128x128.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Nadia</a>
                        <span class="users-list-date">15 Jan</span>
                      </li>
                    </ul>
                   
                  </div>
                  
                  <div class="card-footer text-center">
                    <a href="#">Ver Todos</a>
                  </div>
                  
                </div>
                
            </div>
                  <!-- FIM ANIVERSARIANTES --> 

</div>
    </section>
   
 
@endsection
