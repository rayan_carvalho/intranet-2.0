 
@extends('admin.layouts.app')


@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Informações do Setor</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('setores.index') }}">Setores</a></li>
                    <li class="breadcrumb-item active">Informações do Setor</li>
                </ol>
            </div>
        </div>
    </div>
</section>



<section class="content">      
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-primary ">
                    <div class="card-header">
                        <h3 class="card-title">{{ $setor->name }}</h3>
                    </div>
                    <div class="card-body box-profile">     
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                @foreach($instructions as $instruction)
                                <p>
                                    <a href="<?php echo $instruction->link ?>" target="_bank"><b><?php echo $instruction->descricao ?></b></a>
                                    <a href="#" class="btn btn-info btn-sm float-right"  data-toggle="modal" data-target="#modal-edit-{{ $instruction->id }}"><i class="fa fa-edit"></i></a>
                                    <a href="#" class="btn btn-danger btn-sm float-right" data-toggle="modal" data-target="#modal-trash-{{ $instruction->id }}"><i class="fa fa-trash"></i></a> 
                                </p>
                                
                               
                                @endforeach                                         
                            </li>                       
                        </ul>
                        <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-default"><b><i class="fa fa-plus">&nbsp IT</i></b></a> 
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">            
                                    <div class="modal-header">
                                        <h4>Adicione uma Nova IT</h4>                              
                                    </div>            
                                    <div class="modal-body">
                                        <form role="form" method="POST" action="#">
                                            {!!csrf_field()!!}
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <input type="hidden" name="setor_id" value="{{ $setor->id }}">  
                                                    <label for="descricao">Nome</label>
                                                    <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição da IT">
                                                </div>
                                                <div class="form-group">
                                                    <label for="link">Link</label>
                                                    <input type="text" class="form-control" id="link" name="link" placeholder="Link da IT">
                                                </div>       
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                                <button type="submit" class="btn btn-success">Salvar</button>   
                                        </form>
                                    </div>
                                </div>       
                            </div>        
                        </div>    
                    </div>
                </div>    
            </div>
        </div>

        <div class="col-md-8">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Sobre o Setor</h3>
                </div>                    
                <div class="card-body">                                     
                    <strong><i class="fa fa-pencil mr-1"></i> Contatos</strong>
                    <p class="text-muted">
                        <span class="tag tag-danger">{{ $setor->email }}</span>
                        <span class="tag tag-success"> | </span> 
                        <span class="tag tag-success">{{ $setor->phone }}</span>                        
                    </p>
                    <hr>
                    <strong><i class="fa fa-book mr-1"></i> Supervisor</strong>
                    <p class="text-muted">
                        rayan.carvalho@idj.com.br | (85) 4012-0743
                    </p>
                    <hr>
                    <strong><i class="fa fa-book mr-1"></i> Funcionamento</strong>
                    <p class="text-muted">
                        rayan.carvalho@idj.com.br | (85) 4012-0743
                    </p>
                    <hr>
                </div>              
            </div>
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Colaboradores</h3>
                </div>                   
                <div class="card-body">
                    <strong><i class="fa fa-book mr-1"></i><a href=""> Rayan Carvalho</a></strong>
                    <p class="text-muted">
                        rayan.carvalho@idj.com.br | (85) 4012-0743
                    </p>
                    <hr>
                    <strong><i class="fa fa-book mr-1"></i><a href=""> Rayan Carvalho</a></strong>
                    <p class="text-muted">
                        rayan.carvalho@idj.com.br | (85) 4012-0743
                    </p>
                    <hr>
                </div>                    
            </div>              
        </div>        
    </div>
</div>      
</section>
@endsection

