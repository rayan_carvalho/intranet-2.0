@extends('admin.layouts.app')


@section('content')


<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Novo Setor</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('setores.index') }}">Setores</a></li>
                    <li class="breadcrumb-item active">Novo Setor</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('admin.includes.alerts')

<section class="content">
    <div class="row">
        <div class="col-12">        
            <div class="card">              
                <div class="card-body">
                    <form role="form" method="POST" action="{{ route('setores.store')}}">                     
                        @include('admin.setor._form')                           
                        <button type="submit" class="btn btn-success">Cadastrar</button>   
                    </form>
                </div>
            </div>          
        </div>       
    </div>     
</section>

@endsection
