@extends('admin.layouts.app')



@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Setores IDJ</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
              <li class="breadcrumb-item active">Setores</li>
            </ol>
          </div>
        </div>
      </div>
</section>


@include('sweet::alert')
  
    <section class="content">
      <div class="row">
        <div class="col-12">        
          <div class="card">
            
              <div class="card-body">
                  <a class="btn btn-outline-info float-left" href="{{route('setores.create')}}" ><i class="fa fa-plus"></i> Novo</a>
              </div>
                        
            <div class="card-body">
              <table id="example1" class="table table-hover">
                <thead>
                <tr>
                  <th>Setor</th>
                  <th style="width: 60px">Ações</th>                                        
                </tr>
                </thead>

                  <tbody>   

                     @foreach($setores as $setor)

                     <tr>
                        <td>   
                            <strong> {{ $setor->nome }}</strong>
                            <p class="text-muted">
                                <span class="description">Telefones: {{ $setor->email }} | E-mail: {{ $setor->email }}</span>
                            </p>                   
                        </td>

                        <td>
                            <a class="btn btn-outline-info" href="{{route('setores.show',$setor->id)}}"><i class="fa fa-eye"></i></a>
                        </td>                                                     
                    </tr>

                           
                    @endforeach           

                </tbody>

                <tfoot>

                    <tr>
                      <th>Setor</th>
                      <th>Ações</th>                                    
                    </tr style="width: 60px">

                </tfoot>

              </table>
            </div>          
          </div>         
        </div>      
      </div>      
    </section>



@endsection