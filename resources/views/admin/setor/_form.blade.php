                
@php

if($unidade){
$unidade = $unidade;
}
else {
    $unidade = '';
}

@endphp 

{!!csrf_field()!!}

<input type="hidden" name="id" value="{{$setor->id}}">


<div class="form-group">
    <label for="nome">Nome</label>
    <input type="text" class="form-control" id="nome" name="nome" value="{{old('nome',$setor->nome)}}">
</div>

<div class="form-group">
    <label for="telefone">Telefones</label>
    <input type="text" class="form-control" id="telefone" name="telefone" value="{{old('telefone',$setor->telefone)}}">
</div>

<div class="form-group">
    <label for="email">E-mail</label>
    <input type="email" class="form-control" id="email" name="email" value="{{old('email',$setor->email)}}">
</div>


<div class="form-group">
        <label for="unidade">Unidade</label>
        <select class="form-control" name="unidade" id="unidade">          
            
            @if($unidade)
            <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
            
            @else
            <option value="">Selecione</option>
            @endif

            @foreach($unidades as $unidade)
            <option value="{{ $unidade->id }}" {{old('unidade',$unidade->nome) == $unidade->id ?'selected="selected"': ''}}>{{ $unidade->nome }}</option>
            @endforeach
    
        </select>
    </div>