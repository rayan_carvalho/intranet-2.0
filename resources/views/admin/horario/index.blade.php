@extends('layouts.app')

@extends('layouts.menu')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Horários IDJ</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Horários</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('includes.alerts')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12"> 

            <div class="card">

                @include('horario.modal-form-add')


                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-hover"">
                        <thead>
                            <tr>
                                <th>Descrição dos Horários</th> 
                                <th>Ações</th>                                        
                            </tr>
                        </thead>
                        <tbody>   

                            @foreach($horarios as $horario)

                            <tr>
                                <td>                     
                                    <i class="nav-icon fas fa-clock"></i> 
                                    <span class="username">{{ $horario->operation }}</span>                   

                                </td>

                                <td>  

                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-edit-{{ $horario->id }}"><i class="fa fa-edit"></i></button> 

                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-trash-{{ $horario->id }}"><i class="fa fa-trash"></i></button> 

                                </td>                                                     
                            </tr>  

                            @include('horario.modal-form-options')

                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Descrição dos Horários</th>
                                <th>Ações</th>                                  
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection
