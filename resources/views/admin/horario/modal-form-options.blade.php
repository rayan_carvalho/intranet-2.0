  

                




      <div class="modal fade" id="modal-trash-{{ $horario->id }}">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                
                <div class="modal-header">
                  <h4>Cuidado!</h4>                              
                </div>
                
                <div class="modal-body">
                  <form role="form" method="POST" action="{{ route('horarios.destroy', $horario->id)}}">
                      
                      {!!csrf_field()!!}

                <center>
                    <h2>Tem certeza que quer excluir o horário?</h2>
                    <h3>"{{ $horario->operation }}"</h3>
                    <br>
                    <hr>
                </center> 
                      
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-danger">Excluir</button>              
                  </form>
                </div>
                
              </div>         
            </div>        
      </div>
      
                                        
      <div class="modal fade" id="modal-edit-{{ $horario->id }}">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">            
                <div class="modal-header">
                  <h4>Editar Horário</h4>                              
                </div>
                
                <div class="modal-body">
                  <form role="form"  action="{{ route('horarios.update', $horario->id)}}" method="post">
             
                    
                    {!!csrf_field()!!}

                    <div class="card-body">
                      <div class="form-group">
                        <label for="operation">Descrição</label>
                        <input type="text" class="form-control" id="operation" name="operation" placeholder="Horário" value="{{ $horario->operation }}">
                      </div>    
                      
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-success">Alterar</button>              
                  </form>
                </div>            
              </div>         
            </div>        
      </div>