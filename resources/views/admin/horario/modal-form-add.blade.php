 <div class="card-body">

                <button type="button" class="btn btn-primary btn-plus" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i> Adiconar Horário</button> 

                    
  <div class="modal fade" id="modal-default">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            
            <div class="modal-header">
              <h4>Adicione um Novo Horário</h4>                              
            </div>
            
            <div class="modal-body">
              <form role="form" method="POST" action="{{ route('horarios.store')}}">
                
                {!!csrf_field()!!}

                <div class="card-body">
                  <div class="form-group">
                    <label for="operation">Descrição</label>
                    <input type="text" class="form-control" id="operation" name="operation" placeholder="Horário">
                  </div>                      
                  <button type="submit" class="btn btn-success">Salvar</button> 
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                         
              </form>
            </div>

            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


            
</div>  




