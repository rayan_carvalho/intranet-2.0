<!DOCTYPE html>


@include('admin.layouts.head')

@include('admin.layouts.menu')

<body class="hold-transition sidebar-mini">
        <div class="content-wrapper">  
        

        @yield('content')
        
        </div>
</body>



@include('admin.layouts.footer')
</html>