
 
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>     
    </ul>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4">
  
    <a href="#" class="brand-link">
      <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Ragnar</span>
    </a>

    <div class="sidebar">
    
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="perfil.html" class="d-block">{{ auth()->user()->name }}</a>
        </div>
      </div>
      
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">         
          <li class="nav-item">            
            <a href="{{route('home.index')}}" {!! Request::segment(2) == 'home' ? 'class="nav-link active"' : 'class="nav-link"' !!}>
              <i class="nav-icon fa fa-tachometer-alt"></i>
              <p>Home</p>
            </a>
          </li>          

        <li class="nav-header">ADMINISTRATIVO</li>
         
      
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-id-card"></i>
              <p>Colaboradores</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('unidades.index')}}" {!! Request::segment(2) == 'unidades' ? 'class="nav-link active"' : 'class="nav-link"' !!}>
              <i class="nav-icon fas fa-id-card"></i>
              <p>Unidades</p>
            </a>
          </li> 

          <li class="nav-item">
              <a href="{{route('setores.index')}}" {!! Request::segment(2) == 'setores' ? 'class="nav-link active"' : 'class="nav-link"' !!}>
                <i class="nav-icon fas fa-id-card"></i>
                <p>Setores</p>
              </a>
            </li>  
  
          <li class="nav-header">CONFIGURAÇÕES</li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-cogs"></i>
              <p>Geral</p>
            </a>
          </li>

        </ul>
      </nav>  
    </div>
  
  </aside>
