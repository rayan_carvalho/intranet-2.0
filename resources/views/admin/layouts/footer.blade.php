<footer class="main-footer">
  <strong>Copyright &copy; 2002-2018 <a href="http://www.idj.com.br">Instituto Dom José de Educação e Cultura</a>.</strong>
  All rights reserved.
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> 2.0
  </div>
</footer>  
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>


<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

<script>

$(function () {
  $("#example1").DataTable();
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false
  });
});



$(document).ready(function () {

  function limpa_cep() {
      // Limpa valores do formulário de cep.
      $("#logradouro").val("");
      $("#bairro").val("");
      $("#cidade").val("");
      $("#uf").val("");

  }

  //Quando o campo cep perde o foco.
  $("#cep").blur(function () {

      //Nova variável "cep" somente com dígitos.
      var cep = $(this).val().replace(/\D/g, '');

      //Verifica se campo cep possui valor informado.
      if (cep != "") {

          //Expressão regular para validar o CEP.
          var validacep = /^[0-9]{8}$/;

          //Valida o formato do CEP.
          if (validacep.test(cep)) {

              //Preenche os campos com "..." enquanto consulta webservice.
              $("#logradouro").val("Aguarde..");
              $("#bairro").val("Aguarde..");
              $("#cidade").val("Aguarde..");
              $("#uf").val("Aguarde..");


              //Consulta o webservice viacep.com.br/
              $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                  if (!("erro" in dados)) {
                      //Atualiza os campos com os valores da consulta.
                      $("#logradouro").val(dados.logradouro);
                      $("#bairro").val(dados.bairro);
                      $("#cidade").val(dados.localidade);
                      $("#uf").val(dados.uf);

                  } //end if.
                  else {
                      //CEP pesquisado não foi encontrado.
                      limpa_cep();
                      alert("CEP NÃO ENCONTRADO!");
                  }
              });
          } //end if.
          else {
              //cep é inválido.
              limpa_cep();
              alert("FORMATO DE CEP INVÁLIDO");
          }
      } //end if.
      else {
          //cep sem valor, limpa formulário.
          limpa_cep();
      }
  });

});

</script>

<script type="text/javascript">
  
  function CEP(cep) {

          if(cep.value.length == 5)
              cep.value = cep.value + '-';

  }


 function CPF(cpf) {

          if(cpf.value.length == 3)
              cpf.value = cpf.value + '.';

          if(cpf.value.length == 7)
              cpf.value = cpf.value + '.'; 

          if(cpf.value.length == 11)
              cpf.value = cpf.value + '-';


  }
  
  function Celular(celular) {

            
          if(celular.value.length == 0)                
                   celular.value = '(' + celular.value;          
          
          if(celular.value.length == 3)
                   celular.value = celular.value + ')';

          if(celular.value.length == 5)
                   celular.value = celular.value + ' ';  
   
          if(celular.value.length == 10)
                   celular.value = celular.value + '-';


  
  }

  function Telefone(telefone) {

        
      if(telefone.value.length == 0)                
               telefone.value = '(' + telefone.value;          
      
      if(telefone.value.length == 3)
               telefone.value = telefone.value + ')';
 
      if(telefone.value.length == 8)
               telefone.value = telefone.value + '-';


  
}

</script>