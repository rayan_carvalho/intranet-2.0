                
@php
$horario = $unidade->horario;
@endphp 
 

{!!csrf_field()!!}

<input type="hidden" name="id" value="{{$unidade->id}}">

<div class="form-group">
    <label for="nome">Nome</label>
    <input type="text" class="form-control" id="nome" name="nome" value="{{old('nome',$unidade->nome)}}" >
</div>

<div class="form-group">
    <label for="telefone">telefone</label>
    <input type="text" class="form-control" id="telefone" name="telefone" value="{{old('telefone',$unidade->telefone)}}">
</div>

<div class="form-group">
    <label for="email">E-mail</label>
    <input type="email" class="form-control" id="email" name="email" value="{{old('email',$unidade->email)}}">
</div>

<div class="form-group">
    <label for="horario">Horário de Funcionamento</label>
    <select class="form-control" name="horario" id="horario">
        @if($horario)
        <option value="{{ $horario->id }}">{{ $horario->horario }}</option>
        
        @else
        <option value="">Selecione</option>
        @endif

        @foreach($horarios as $horario)
        <option value="{{ $horario->id }}" {{old('horario',$horario->horario) == $horario->id ?'selected="selected"': ''}}>{{ $horario->horario }}</option>
        @endforeach

    </select>
</div>

<div class="form-group">
    <label for="localizacao">Localização</label>
    <input type="text" class="form-control" id="localizacao" name="localizacao" value="{{old('localizacao',$unidade->localizacao)}}" placeholder="Iframe de Localização">
    <p>Observação: <code>Deve ser obtido no google maps o iframe com o código da localização no tamanho "Médio".</code></p>
</div>
