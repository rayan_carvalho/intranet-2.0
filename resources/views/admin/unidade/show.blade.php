 
@extends('admin.layouts.app')


@section('content')

@php
$horario = $unidade->horario;

@endphp

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Informações da Unidade</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('unidades.index') }}">Unidades</a></li>
                    <li class="breadcrumb-item active">Informações da Unidade</li>
                </ol>
            </div>
        </div>
    </div>
</section>
@include('sweet::alert')

<section class="content">
    <div class="container-fluid">
        
        <div class="row">


            <div class="col-md-12">
                <div class="card card-widget widget-user">     
                    <div class="widget-user-header text-white" style="background: url('../../dist/img/photo1.png') center center;">                
                        <h5 class="widget-user-desc">{{ $unidade->nome }} </h5>

                        <button  class="btn btn-danger float-right" data-toggle="modal" data-target="#destroy{{ $unidade->id }}"><i class="fa fa-trash"></i> Excluir</button>
                        <a class="btn btn-info float-right" href="{{route('unidades.edit',$unidade->id)}}"><i class="fa fa-edit"></i>Editar</a>
                       
                    </div>

                    <div class="card-footer">

                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">Horário de Funcionamento</h5>                      
                                    <span class="tag tag-success">{{ $horario->horario }}</span>  
                                </div>              
                            </div> 

                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">Telefones</h5>
                                    <span class="tag tag-success">{{ $unidade->telefone }}</span>  
                                </div>                
                            </div>

                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header">E-mail</h5>
                                    <span class="tag tag-success">{{ $unidade->email }}</span> 
                                </div>                  
                            </div> 

                        </div>
                    </div>
                </div>   
            </div>


            <div class="col-md-5">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Colaboradores</h3>
                    </div>

                    <div class="card-body">

                        <div class="user-block">
                            <img class="img-circle" src="{{ asset('../../dist/img/user1-128x128.jpg')}}" >
                            <span class="username"><a href="#">DTI</a></span>
                            <span class="description">Telefones: (85) 4012-0725 | E-mail: dti@idj.com.br</span> 
                        </div> 
                        <hr> 

                        <div class="user-block">
                            <img class="img-circle" src="{{ asset('../../dist/img/user1-128x128.jpg')}}" >
                            <span class="username"><a href="#">DTI</a></span>
                            <span class="description">Telefones: (85) 4012-0725 | E-mail: dti@idj.com.br</span> 
                        </div> 
                        <hr> 

                        <div class="user-block">
                            <img class="img-circle" src="{{ asset('../../dist/img/user1-128x128.jpg')}}" >
                            <span class="username"><a href="#">DTI</a></span>
                            <span class="description">Telefones: (85) 4012-0725 | E-mail: dti@idj.com.br</span> 
                        </div> 
                        <hr> 

                    </div>                    
                </div>
            </div>


            <div class="col-md-7">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Localização</h3>
                    </div>                   
                    <center>  
                        <?php echo $unidade->localizacao ?>
                    </center>                                
                </div>
            </div>




        </div>
</section>
@endsection
