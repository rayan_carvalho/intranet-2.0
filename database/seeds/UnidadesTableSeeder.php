<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Unidade;

class UnidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Unidade::create([
            'nome'      => 'IDJ - Sede',   
            'telefone'      => '(85)4012-0725',   
            'email'      => 'dti@idj.com.br',   
            'horario_id'      => '1', 
            'localizacao'      => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1990.359373622864!2d-38.62130689374559!3d-3.870384884801781!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c75311fd6f8313%3A0xebf1ef50d85fbf9c!2sR.+Ouro+Preto%2C+5+-+Coqueiral%2C+Maracana%C3%BA+-+CE%2C+61902-035!5e0!3m2!1spt-BR!2sbr!4v1529006148693" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',            
        ]);
        
          Unidade::create([
            'nome'      => 'IDJ - Maracanaú',   
            'telefone'      => '(85)4012-0725',   
            'email'      => 'dti@idj.com.br',   
            'horario_id'      => '1', 
            'localizacao'      => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1990.359373622864!2d-38.62130689374559!3d-3.870384884801781!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c75311fd6f8313%3A0xebf1ef50d85fbf9c!2sR.+Ouro+Preto%2C+5+-+Coqueiral%2C+Maracana%C3%BA+-+CE%2C+61902-035!5e0!3m2!1spt-BR!2sbr!4v1529006148693" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',       
            
        ]);
    }
}
