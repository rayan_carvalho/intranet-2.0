<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Horario;

class HorariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          Horario::create([
            'horario'      => 'Segunda a Sexta de 08:00 às 18:00',            
        ]);
        
          Horario::create([
            'horario'      => 'Segunda a Domingo de 08:00 às 18:00',
            
        ]);
    }
}
