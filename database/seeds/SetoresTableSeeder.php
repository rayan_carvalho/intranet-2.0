<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Setor;


class SetoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           Setor::create([
            'nome'      => 'DTI',   
            'telefone'      => '(85)4012-0725',   
            'email'      => 'dti@idj.com.br',   
            'unidade_id'      => '1',
              
                 
        ]);
        
          Setor::create([
            'nome'      => 'RH',   
            'telefone'      => '(85)4012-0742',   
            'email'      => 'rh@idj.com.br', 
            'unidade_id'      => '1', 
                       
        ]);
    }
}
