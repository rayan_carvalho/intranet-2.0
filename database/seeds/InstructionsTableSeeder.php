<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Instruction;

class InstructionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Instruction::create([
            'descricao'      => 'PROCESSO DE BACKUP',  
            'link'           => 'https://tableless.com.br/iniciando-no-git-parte-2/', 
            'setor_id'      => '1',            
        ]);
    }
}
