<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnidadeValidationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->get("id");

        //dd($id);

        return [

            'nome' => "required|string|min:5|max:100|unique:unidades,nome,$id,id",
            'horario' => 'required',
            'localizacao' => 'required|string|min:20',


        ];
    }
}
