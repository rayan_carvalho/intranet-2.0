<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Setor;
use App\Models\Admin\Unidade;
use App\Models\Admin\Instruction;
use App\Http\Controllers\Controller;
use App\Http\Requests\SetorValidationFormRequest;
use DB;
use Alert;


class SetorController extends Controller
{
    public function index()
    {


        $setores = Setor::get();


        return view('admin.setor.index', compact('setores'));
    }

    public function create()
    {
       // $setores = Setor::get();
        $unidades = Unidade::get();
        $unidade = '';
        
        return view('admin.setor.create', ['setor' => new Setor()], compact('unidades','unidade'));
    }



    public function store(SetorValidationFormRequest $request)
    {

        $data = $request->all();
        unset($data['unidade']);
        $data['unidade_id'] = $request->unidade;


        $response = Setor::create($data);



        if ($response) {
            Alert::success('', 'Sucesso!');
            return redirect('admin/setores');

        } else {
            Alert::error('', 'Erro!');
            return redirect('admin/setores');
        }



    }

    public function show($id)
    {
        $setor = Setor::findOrFail($id);
        $instructions =  $setor->instruction;
                      

        return view('admin.setor.show', compact('setor','instructions'));
    }


    public function edit($id)
    {
        $setor = Setor::findOrFail($id);
        $unidades = Unidade::get()->where('id', '<>', $setor->unidade_id);
        $unidade = Unidade::findOrFail($setor->unidade_id);


        return view('admin.setor.edit', compact('setor', 'unidades','unidade'));
    }



    public function update(Request $request, $id)
    {


        $setor = Setor::findOrFail($id);

        $data = $request->all();

        unset($data['unidade']);

        $data['horario_id'] = $request->unidade;        

        $setor->fill($data);

        $response = $setor->save();


        if ($response) {

            Alert::success('', 'Sucesso!');
            return redirect('admin/setores/' . $setor->id);

        } else {
            Alert::error('', 'Erro!');
            return redirect('admin/setores' . $setor->id);
        }


    }

    public function destroy($id)
    {
        $setor = Setor::find($id);
        if (!$setor) {
            abort(404);
        }

        $response = $setor->delete();

        return redirect()
            ->route('setores.index')
            ->with('success', 'Setor excluído com Sucesso!');


        return redirect()
            ->route('setores.index')
            ->with('error', 'Erro ao Excluir o Setor!');


    }



}