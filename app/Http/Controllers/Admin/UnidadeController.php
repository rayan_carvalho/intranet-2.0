<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Unidade;
use App\Models\Admin\Horario;
use App\Http\Requests\UnidadeValidationFormRequest;
use DB;
use Alert;


class UnidadeController extends Controller
{
    public function index()
    {

        $horarios = Horario::get();

        $unidades = DB::table('unidades')
            ->join('horarios', 'unidades.horario_id', '=', 'horarios.id')
            ->select('unidades.*', 'horarios.horario')
            ->get();


        return view('admin.unidade.index', compact('unidades', 'horarios'));
    }

    public function create()
    {
        $horarios = Horario::get();
        $horario = '';
        return view('admin.unidade.create', ['unidade' => new Unidade()], compact('horarios','horario'));
    }



    public function store(UnidadeValidationFormRequest $request)
    {

        $data = $request->all();
        unset($data['horario']);
        $data['horario_id'] = $request->horario;


        $response = Unidade::create($data);



        if ($response) {
            Alert::success('', 'Sucesso!');
            return redirect('admin/unidades');

        } else {
            Alert::error('', 'Erro!');
            return redirect('admin/unidades');
        }


    }

    public function show($id)
    {
        $unidade = Unidade::findOrFail($id);

              

        return view('admin.unidade.show', compact('unidade'));
    }

    public function edit($id)
    {
        $unidade = Unidade::findOrFail($id);
        $horarios = Horario::get()->where('id', '<>', $unidade->horario_id);
        $horario = Horario::findOrFail($unidade->horario_id);


        return view('admin.unidade.edit', compact('unidade', 'horarios', 'horario'));
    }



    public function update(UnidadeValidationFormRequest $request, $id)
    {

        $unidade = Unidade::findOrFail($id);

        $data = $request->all();

        unset($data['horario']);

        $data['horario_id'] = $request->horario;        

        $unidade->fill($data);

        $response = $unidade->save();


        if ($response) {

            Alert::success('', 'Sucesso!');
            return redirect('admin/unidades/' . $unidade->id);

        } else {
            Alert::error('', 'Erro!');
            return redirect('admin/unidades' . $unidade->id);
        }

    }

    public function destroy($id)
    {
        $unidade = Unidade::find($id);
        if (!$unidade) {
            abort(404);
        }

        $response = $unidade->delete();

        return redirect()
            ->route('unidades.index')
            ->with('success', 'Unidade excluída com Sucesso!');


        return redirect()
            ->route('unidades.index')
            ->with('error', 'Erro ao Excluir a Unidade!');


    }
}
