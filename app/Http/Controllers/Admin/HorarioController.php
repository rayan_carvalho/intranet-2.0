<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Horario;
use App\Http\Controllers\Controller;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $horarios = Horario::get();
         

        return view('horario.index', compact('horarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
       
        $horario = New Horario();
        $horario->operation = $request->operation;       
        $response = $horario->save();
                

           if($response)       
             
            
             return redirect ()
            ->route('horarios.index')
             ->with('success','Horário salvo com sucesso!');
            
        
            return redirect()
             ->route('horarios.index')
             ->with('error','Erro ao salvar o horário!');
            
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    
         $horario = Horario::findOrFail($id);

         $horario->operation = $request->operation;

         $response = $horario->save();
       
         if($response)
        
             
            
             return redirect ()
            ->route('horarios.index')
             ->with('success','Sucesso ao Editar!');
            
        
            return redirect()
             ->route('horarios.index')
             ->with('error','Erro ao Editar!');
        
    

    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $horario = Horario::find($id);
        if(!$horario){
            abort(404);
        }

       $response = $horario->delete();

         return redirect ()
            ->route('horarios.index')
             ->with('success','Horário excluído com Sucesso!');
            
        
            return redirect()
             ->route('horarios.index')
             ->with('error','Erro ao Excluir o Horário!');
        
    
    }
}
