<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Horario;
use App\Models\Admin\Unidade;
use App\Models\Admin\Instruction;

class Setor extends Model
{
     protected $fillable = [
        'nome', 'telefone','email','unidade_id'
    ];



	

    public function unidade(){

      return $this->hasOne(Unidade::class);
    }



    public function instruction(){

      return $this->hasMany(Instruction::class);
    }


  


}