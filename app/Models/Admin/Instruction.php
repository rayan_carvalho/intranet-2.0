<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Setor;


class Instruction extends Model
{
   protected $fillable = ['descricao','link'];


   public function setor(){

      return $this->hasOne(Setor::class);
    }



}