<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;


class Horario extends Model {

    protected $fillable = ['horario'];
    protected $guarded = ['id', 'created_at', 'update_at'];

    public function Unidade(){

        return $this->belongsTo(Unidade::class);
    }
  
}
