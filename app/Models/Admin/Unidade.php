<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Horario;

class Unidade extends Model
{
    
 protected $fillable = [
        'nome', 'telefone','email','localizacao','horario_id'
    ];


    public function horario(){

        return $this->belongsTo(Horario::class);
      }
	



}
